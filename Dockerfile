FROM perl
WORKDIR /opt/kubequeue
COPY . .
RUN cpanm --notest .; chmod +x kubequeue
EXPOSE 3000
CMD ./kubequeue prefork
