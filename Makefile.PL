use strict;
use warnings;

use ExtUtils::MakeMaker;

WriteMakefile(
  VERSION   => '0.01',
  PREREQ_PM => {
    'Clone' => 0,
    'Data::Compare' => 0,
    'DateTime' => 0,
    'DateTime::Format::ISO8601' => 0,
    'JSON::Path' => 0,
    'Mojolicious' => '9.26',
    'Moose' => 0,
    'MooseX::Singleton' => 0,
    'String::Random' => 0,
    'YAML::XS' => 0,
  },
  test => {TESTS => 't/*.t'}
);
