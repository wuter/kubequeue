package KubeObject::UA;

use v5.34;

use MooseX::Singleton;
use Mojo::UserAgent;
use Mojo::URL;
use Mojo::JSON qw/encode_json/;

use feature "signatures";
no warnings "experimental::signatures";

has apiurl => (
	is => 'rw',
	isa => 'Maybe[Mojo::URL]',
	lazy => 1,
	builder => "_build_apiurl",
);

has _logger => (
	is => 'rw',
	isa => 'CodeRef',
	writer => 'set_logger',
	traits => ['Code'],
	default => sub {
		sub { }
	},
	handles => {
		log => 'execute',
	},
);

sub _build_apiurl {
	if(exists($ENV{KUBERNETES_SERVICE_HOST}) && exists($ENV{KUBERNETES_SERVICE_PORT})) {
		return Mojo::URL->new->scheme("https")->host($ENV{KUBERNETES_SERVICE_HOST})->port($ENV{KUBERNETES_SERVICE_PORT});
	}
	die "don't have an API URL, can't continue!";
}

sub urlfor($self, $url, $method="") {
	my $rv = Mojo::URL->new;
	$rv->base($self->apiurl);
	$rv->path($url);
	my $absurl = Mojo::URL->new($url)->base($self->apiurl)->to_abs;
	$self->log("$method " . ($absurl->to_string));
	return $absurl;
}

has sadir => (
	is => 'rw',
	isa => 'Str',
	default => "/run/secrets/kubernetes.io/serviceaccount",
	trigger => sub { my $self = shift; $self->_clear_ua; $self->_clear_token },
);

has token => (
	is => 'ro',
	isa => 'Str',
	lazy => 1,
	builder => '_build_token',
	clearer => '_clear_token',
);

sub _build_token ($self) {
	my $rv;
	{
		local $/;
		open my $token_f, "<" . $self->sadir . "/token";
		$rv = <$token_f>;
	}
	chomp $rv;
	return $rv;
}

has 'ua' => (
	is => 'ro',
	isa => 'Mojo::UserAgent',
	lazy => 1,
	builder => '_build_ua',
	clearer => '_clear_ua',
);

sub _build_ua ($self) {
	my $ua = Mojo::UserAgent->new(max_response_size => 0, ca => $self->sadir . "/ca.crt");
	$ua->on(start => sub($ua, $tx) {
		$tx->req->headers->header(Authorization => "Bearer " . $self->token);
	});
	return $ua;
}

sub fetch($self, @elements) {
	return $self->ua->get($self->urlfor("/" . join("/", @elements), "GET"));
}

sub fetch_query($self, $query, $elements) {
	return $self->ua->get($self->urlfor("/", join("/", @$elements), "GET")->query(%$query));
}

sub watch($self, $handle, $resourceVersion, @elements) {
	my $tx = $self->ua->build_tx(GET => $self->urlfor("/" . join("/", @elements), "watch")->query(watch => "true", resourceVersion => $resourceVersion));
	$tx->res->content->unsubscribe('read')->on(read => $handle);
	$self->ua->start($tx => sub($ua, $tx) { $self->log("$tx done") });
}

sub post($self, $elements, $body) {
	return $self->ua->post($self->urlfor('/' . join('/', @$elements), "POST") => json => $body);
}

sub put($self, $elements, $body) {
	return $self->ua->put($self->urlfor("/" . join("/", @$elements), "PUT") => json => $body);
}

sub delete($self, $elements) {
	return $self->ua->delete($self->urlfor("/" . join("/", @$elements), "DELETE"));
}

no Moose;

package KubeObject;

use Moose::Role;

use feature "signatures";
no warnings "experimental::signatures";

use Mojo::JSON qw/decode_json/;
use JSON::Path;
use Data::Compare;
use DateTime;
use DateTime::Format::ISO8601;

requires 'pluralkind';
requires 'group';
requires 'version';

has 'apiroot' => (
	is => 'ro',
	isa => 'ArrayRef[Str]',
	lazy => 1,
	builder => '_build_apiroot',
);

sub _build_apiroot($self) {
	return ['apis', $self->group, $self->version, $self->pluralkind];
}

has 'apiroot_ns' => (
	is => 'ro',
	isa => 'ArrayRef[Str]',
	lazy => 1,
	builder => '_build_apiroot_ns',
);

sub _build_apiroot_ns($self) {
	if(!$self->has_namespace) {
		return $self->apiroot;
	}
	return ['apis', $self->group, $self->version, 'namespaces', $self->namespace, $self->pluralkind];
}

has 'kubeobj' => (
	is => 'ro',
	isa => 'HashRef',
	lazy => 1,
	builder => '_build_kubeobj',
	trigger => \&_trigger_changes,
	predicate => '_has_kubeobj',
	clearer => '_clear_kubeobj',
);

sub _build_kubeobj($self) {
	my @elements = ( @{$self->apiroot_ns} );
	push @elements, $self->name;
	my $res = KubeObject::UA->instance->fetch(@elements)->res;
	die "could not fetch object: " . $res->message unless $res->is_success;
	return $res->json;
}

sub _trigger_changes($self, $new, $old = undef) {
	if($self->name ne $new->{metadata}{name}) {
		die "name does not match";
	}
	foreach my $trigger(keys %{$self->triggers}) {
		my $jpath = JSON::Path->new($trigger);
		my @old_v;
		if(defined($old)) {
			@old_v = $jpath->values($old);
		}
		my @new_v = $jpath->values($new);
		if(!Compare(@old_v, @new_v)) {
			&{$self->triggers->{$trigger}}($self, $new, $old);
		}
	}
}

has 'creationtime' => (
	is => 'ro',
	isa => 'DateTime',
	lazy => 1,
	builder => '_build_creationtime',
);

sub _build_creationtime($self) {
	return DateTime::Format::ISO8601->parse_datetime($self->kubeobj->{metadata}{creationTimestamp});
}

has 'name' => (
	is => 'ro',
	isa => 'Str',
	required => 1,
);

has 'namespace' => (
	is => 'ro',
	isa => 'Str',
	predicate => 'has_namespace',
);

has 'deleteme' => (
	is => 'rw',
	isa => 'CodeRef',
	predicate => 'has_deleteme',
);

has 'triggers' => (
	is => 'rw',
	isa => 'HashRef[CodeRef]',
	traits => ['Hash'],
	default => sub { {} },
	handles => {
		set_trigger => 'set',
		get_trigger => 'get',
		has_trigger => 'exists',
	},
);

has 'is_backed' => (
	is => 'ro',
	isa => 'Bool',
	lazy => 1,
	builder => '_build_is_backed',
);

sub _build_is_backed($self) {
	my $obj;
	eval {
		$obj = $self->kubeobj;
	};
	return defined($obj);
}

sub get_all($class) {
	my $arr = [];
	my @elements = ('apis', $class->group, $class->version, $class->pluralkind);
	my $res = KubeObject::UA->instance->fetch(@elements)->res;
	die "could not fetch objects: " . $res->message unless $res->is_success;
	my $data = $res->json;
	my $rsrcV = $data->{metadata}{resourceVersion};
	foreach my $obj(@{$data->{items}}) {
		my %opts = ( kubeobj => $obj, name => $obj->{metadata}{name} );
		if(exists($obj->{metadata}{namespace})) {
			$opts{namespace} = $obj->{metadata}{namespace};
		}
		push @$arr, $class->new(%opts);
	}
	return ($rsrcV, $arr);
}

sub get_labeled($class, $label) {
	my @elements = ('apis', $class->group, $class->version, $class->pluralkind);
	my $res = KubeObject::UA->instance->fetch_query(\@elements, { labelSelector => $label });
	die "could not fetch objects: " . $res->message unless $res->is_success;
	my $data = $res->json;
	my $rsrcV = $data->{metadata}{resourceVersion};
	my $arr = [];
	foreach my $obj(@{$data->{items}}) {
		my %opts = ( kubeobj => $obj, name => $obj->{metadata}{name} );
		if(exists($obj->{metadata}{namespace})) {
			$opts{namespace} = $obj->{metadata}{namespace};
		}
		push @$arr, $class->new(%opts);
	}
	return ($rsrcV, $arr);
}

sub get_ownedby($class, $owner, $namespace = undef) {
	my @elements = ('apis', $class->group, $class->version, (defined($namespace) ? ('namespaces', $namespace, $class->pluralkind) : $class->pluralkind));
	my $res = KubeObject::UA->instance->fetch(@elements);
	die "could not fetch objects: " . $res->message unless $res->is_success;
	my $data = $res->json;
	my $arr = {};
	foreach my $obj(@{$data->{items}}) {
		if(exists($obj->{metadata}{ownerReferences}) && $obj->{metadata}{ownerReferences}{uid} eq $owner->kubeobj->{metadata}{uid}) {
			my %opts = ( kubeobj => $obj, name => $obj->{metadata}{name} );
			if(exists($obj->{metadata}{namespace})) {
				$opts{namespace} = $obj->{metadata}{namespace};
			}
			$arr->{$obj->{metadata}{name}} = $class->new(%opts);
		}
	}
	return ($data->{metadata}{resourceVersion}, $arr);
}

sub watch_all($class, $resourceVersion, $method) {
	my @elements = ('apis', $class->group, $class->version, $class->pluralkind);
	return KubeObject::UA->instance->watch(sub($content, $bytes){if(defined($bytes) && length($bytes) > 0){&$method(decode_json($bytes))}}, $resourceVersion, @elements);
}

sub watch($self, $method) {
	die "need a deleteme sub" unless $self->has_deleteme;
	my $elements = $self->apiroot_ns;
	push @$elements, $self->name;
	my $ua = KubeObject::UA->instance;
	my $handle = sub($data) {
		if($data->{type} eq "ADDED") {
			$ua->log("added");
		} elsif($data->{type} eq "DELETED") {
			$ua->log("removed");
		} elsif($data->{type} eq "MODIFIED") {
			$ua->log($data->{type});
		}
	};
	return $ua->watch(sub($content, $bytes){if(defined($bytes) && length($bytes) > 0){&$method(decode_json($bytes))}}, $self->resourceVersion, @$elements);
}

sub store_new($self) {
	die "Can't store without a kubeobj" unless $self->_has_kubeobj;
	my $elements = $self->apiroot_ns;
	my $req = KubeObject::UA->post($elements, $self->kubeobj);
	die "Could not store: " . $req->res->message unless $req->res->is_success;
	$self->_clear_kubeobj
}

sub store_update($self) {
	die "Can't store without a kubeobj" unless $self->_has_kubeobj;
	my $elements = [ @{ $self->apiroot_ns } ];
	push @$elements, $self->name;
	my $req = KubeObject::UA->put($elements, $self->kubeobj);
	die "Could not store: " . $req->res->message unless $req->res->is_success;
	$self->_clear_kubeobj
}

sub store_status($self) {
	die "Can't store without a kubeobj" unless $self->_has_kubeobj;
	my $elements = [ @{ $self->apiroot_ns } ];
	push @$elements, $self->name, "status";
	my $req = KubeObject::UA->put($elements, $self->kubeobj);
	die "Could not store status: " . $req->res->message unless $req->res->is_success;
	$self->_clear_kubeobj
}

sub kube_delete($self) {
	my $elements = [ @{ $self->apiroot_ns } ];
	push @$elements, $self->name;
	my $req = KubeObject::UA->delete($elements);
	return if $req->res->code == 404;
	die "Could not delete: " . $req->res->message unless $req->res->is_success;
}

1;
